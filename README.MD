# RedisGeo Demo

## 问题描述
### 使用 redis-cli 添加的坐标信息可以在 redis-cli 中用geopos命令查看，但使用 RedisTemplate 添加的坐标信息无法在 redis-cli 中查看，但程序接口能查询到结果，这个问题困扰我两天了，求助~


## 问题重现

### redis-cli 操作(正常)
```
geoadd gis 121.296321 31.716439 a1
geoadd gis 121.296321 31.716439 a2
geopos gis a1 a2
```
![](img/redis-cli-geo.png)


### 调用RedisTemplate接口(看似正常)
- [http://localhost:8228/addByName?x=121.466390&y=31.228006&name=m1](http://localhost:8228/addByName?x=121.466390&y=31.228006&name=m1)
- [http://localhost:8228/queryByName?name=m1](http://localhost:8228/queryByName?name=m1)

#### 图1
![](img/sdk_add.png)

#### 图2
![](img/sdk_query.png)

#### 图3
![](img/sdk_cli_null.png)
RedisTemplate接口添加数据后，redis-cli 查不到数据

## 解决方案

```
@Autowired(required = false)
public void setRedisTemplate(RedisTemplate redisTemplate) {
    RedisSerializer stringSerializer = new StringRedisSerializer();
    redisTemplate.setKeySerializer(stringSerializer);
    redisTemplate.setValueSerializer(stringSerializer);
    redisTemplate.setHashKeySerializer(stringSerializer);
    redisTemplate.setHashValueSerializer(stringSerializer);
    this.redisTemplate = redisTemplate;
}
```

## 参考资料
- [https://blog.csdn.net/skymouse2002/article/details/80736577](https://blog.csdn.net/skymouse2002/article/details/80736577)