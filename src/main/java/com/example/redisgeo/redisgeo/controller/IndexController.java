package com.example.redisgeo.redisgeo.controller;

/**
 * Created by  HanleyTang on 2019-02-27
 */

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.example.redisgeo.redisgeo.model.CodeType;
import com.example.redisgeo.redisgeo.model.Result;
import com.example.redisgeo.redisgeo.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class IndexController {

    static final protected Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private RedisService redisService;


    @RequestMapping("/addStr")
    public Object addStr(@RequestParam(name = "key") String key
            ,@RequestParam(name = "value") String value){

        try {
            redisService.addStr(key,value);
            return new Result(CodeType.ok.value,"添加成功");
        } catch (Exception e) {
            logger.error("添加String信息失败,{}",e);
            return new Result(CodeType.ng.value,CodeType.ng.desc);
        }
    }


    @RequestMapping("/add")
    public Object add(@RequestParam(name = "x") double x
            ,@RequestParam(name = "y") double y){

        try {
            redisService.add(x,y, IdUtil.simpleUUID());
            return new Result(CodeType.ok.value,"添加成功");
        } catch (Exception e) {
            logger.error("添加位置信息失败,{}",e);
            return new Result(CodeType.ng.value,CodeType.ng.desc);
        }
    }



    @RequestMapping("/addByName")
    public Object addByName(@RequestParam(name = "x") double x
            ,@RequestParam(name = "y") double y
            ,@RequestParam(name = "name") String name){

        try {
            redisService.add(x,y, name);
            return new Result(CodeType.ok.value,"添加成功");
        } catch (Exception e) {
            logger.error("添加位置信息失败,{}",e);
            return new Result(CodeType.ng.value,CodeType.ng.desc);
        }
    }


    @RequestMapping("/addByKey")
    public Object addByKey(
            @RequestParam(name = "key") String key
            ,@RequestParam(name = "x") double x
            ,@RequestParam(name = "y") double y
            ,@RequestParam(name = "name",required = false) String name){

        try {
            if (StrUtil.isBlank(name)){
                redisService.add(key,x,y, IdUtil.simpleUUID());
            }else{
                redisService.add(key,x,y, name);
            }
            return new Result(CodeType.ok.value,"添加成功");
        } catch (Exception e) {
            logger.error("添加位置信息失败,{}",e);
            return new Result(CodeType.ng.value,CodeType.ng.desc);
        }
    }



    @RequestMapping("/queryByName")
    public Object queryByName(@RequestParam(name = "name") String ... name){
        try {
            //FIXME 可能涉及 依据Name查询更多关联数据,比如用户昵称,头像等
            Object data = redisService.query(name);
            return new Result(CodeType.ok.value,"查询成功",data);
        } catch (Exception e) {
            logger.error("查询指定Name坐标失败,{}",e);
            return new Result(CodeType.ng.value,"查询指定Name坐标失败");
        }
    }



    @RequestMapping({"/nearByName"})
    public Object nearByName(@RequestParam(name = "name") String name,
                              @RequestParam(name = "value") int distance,
                              @RequestParam(name = "limit") int limit){
        try {
            //FIXME 可能涉及 依据Name查询更多关联数据,比如用户昵称,头像等
            Object data = redisService.nearByName(name,distance,limit);
            return new Result(CodeType.ok.value,"查询成功",data);
        } catch (Exception e) {
            logger.error("查询位置信息失败,{}",e);
            return new Result(CodeType.ng.value,"查询微信信息失败");
        }
    }


    @RequestMapping({"/nearByCoord"})
    public Object nearByCoord(@RequestParam(name = "x") double x,@RequestParam(name = "y") double y){

        try {
            //FIXME 可能涉及 依据Name查询更多关联数据,比如用户昵称,头像等
            Object data = redisService.nearByCoord(x,y,5,100);
            return new Result(CodeType.ok.value,"查询成功",data);
        } catch (Exception e) {
            logger.error("查询附近坐标失败,{}",e);
            return new Result(CodeType.ng.value,"服务异常");
        }

    }


}
