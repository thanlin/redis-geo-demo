package com.example.redisgeo.redisgeo.model;

import java.io.Serializable;

/**
 * Created by  HanleyTang on 2019-02-28
 */
public class Result implements Serializable {

    private String code;
    private String desc;
    private Object data;

    public Result(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Result(String code, String desc, Object data) {
        this.code = code;
        this.desc = desc;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
