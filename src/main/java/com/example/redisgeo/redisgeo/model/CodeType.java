package com.example.redisgeo.redisgeo.model;

public enum CodeType {


    ok("ok","处理成功"),
    ng("ng","处理失败");

    public String value;
    public String desc;

    CodeType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
