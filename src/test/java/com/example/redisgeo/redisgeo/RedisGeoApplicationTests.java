package com.example.redisgeo.redisgeo;

import com.example.redisgeo.redisgeo.service.RedisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisGeoApplicationTests {

    private final String geoKey = "gis";

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    RedisService redisService;


    @Test
    public void testAdd(){
        redisTemplate.opsForGeo().add(geoKey, new Point(121.401576, 31.262819), "上海西站");
        Map map = new HashMap<String, Point>();
        map.put("上海站", new Point(121.455821, 31.249612));
        map.put("虹桥火车站", new Point(121.319819,31.194119));
        map.put("西站附近1", new Point( 121.399564,31.261951));
        map.put("西站附近2", new Point( 121.404864,31.260906));
        map.put("西站附近3", new Point( 121.404413,31.261859));

        redisTemplate.opsForGeo().add(geoKey, map);
    }

    /**
     * 计算两地距离
     */
    @Test
    public void testDistance(){
        Distance distance = redisTemplate.opsForGeo()
                .distance(geoKey,"上海站","上海西站", RedisGeoCommands.DistanceUnit.KILOMETERS);
        System.out.println(distance);
    }



    @Test
    public void testQuery(){
        List<Point> points = redisTemplate.opsForGeo().position(geoKey,"上海站","虹桥火车站","上海西站");
        System.out.println(points);
    }

    /**
     * 查询指定 地点 附近5公里的数据
     */
    @Test
    public void testNearByPlace(){
        Distance distance = new Distance(5, Metrics.KILOMETERS);
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().includeCoordinates().sortAscending().limit(5);
        GeoResults<RedisGeoCommands.GeoLocation<String>> results = redisTemplate.opsForGeo()
                .radius(geoKey,"上海西站",distance,args);
        System.out.println(results);
    }


    /**
     * 查询特定坐标 指定范围 中的数据
     */
    @Test
    public void testNearByCoord(){
        Distance distance = new Distance(5, Metrics.KILOMETERS);
        Circle circle = new Circle(new Point(121.401576, 31.262819),distance);

        //Circle circle = new Circle(116.405285,39.904989, Metrics.KILOMETERS.getMultiplier());
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().includeCoordinates().sortAscending().limit(5);
        GeoResults<RedisGeoCommands.GeoLocation<String>>  results = redisTemplate.opsForGeo()
                .radius(geoKey,circle,args);
        System.out.println(results);
    }



}
